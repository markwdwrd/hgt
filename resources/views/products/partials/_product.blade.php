  <li class="product">
      <a href="{!! url('shop/' . $product->slug) !!}" title="{!! $product->name !!}" class="image" style="background-image: url({!! $product->hasMedia('images') ? $product->getMedia('images')->first()->getUrl() : '' !!});">
      </a>
      <div>
        <h4><a href="{!! url('shop/' . $product->slug) !!}" title="{!! $product->name !!}">{!! $product->name !!}</a></h4>
        @if($product->soldOut)
          <span class="sold-out">Sold Out</span>
        @endif
      </div>
  </li>