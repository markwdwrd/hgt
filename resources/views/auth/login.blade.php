@extends('layouts.master')

@section('page-title'){!! 'Log in' !!}@stop
@section('page-id'){!! 'login' !!}@stop
@section('main-class'){!! 'account login' !!}@stop

@section('introduction')
    <section>
        <h1>Log in to your account</h1>
    </section>
@stop

@section('content')
<div class="container">
    {!! Form::open(['url' => 'login']) !!}
        @csrf
        <fieldset>
            {!! Form::label('email', 'Email') !!}
            {!! Form::text('email', null, ['id' => 'email', 'placeholder' => 'Email']) !!}
            <span class="error">{!! $errors->first('email') !!}</span>
        </fieldset>
        <fieldset>
            {!! Form::label('password', 'Password') !!}
            {!! Form::password('password', ['id' => 'password', 'placeholder' => 'Password']) !!}
            <span class="error">{!! $errors->first('password') !!}</span>
        </fieldset>
        <fieldset>
            <label>
                <input type="checkbox" name="remember" id="remember" {!! old('remember') ? 'checked' : '' !!}>
                Remember Me
            </label>
        </fieldset>
        <fieldset>
            {!! Form::button('Login', ['type' => 'submit', 'class' => 'w-full button green']) !!}
            <p><a href={!! route('password.request') !!} title="Reset your password">Forgot your password?</a></p>
        </fieldset>
    {!! Form::close() !!}
    <p>Don't have an account? <a href="{!! url('register') !!}">Sign up for one here</a>.</p>
    @include('partials._messages')
</div>
@stop
