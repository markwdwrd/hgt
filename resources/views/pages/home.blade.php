@extends('layouts.master')

@section('page-title'){!! $cmsPage->title !!}@stop
@section('meta-title'){!! $cmsPage->meta_title !!}@stop
@section('meta-keywords'){!! $cmsPage->meta_keywords !!}@stop
@section('meta-description'){!! $cmsPage->meta_description !!}@stop
@section('page-id'){!! $cmsPage->slug !!}@stop
@section('main-class'){!! 'home' !!}@stop

@section('slider')
    @include('pages.partials._slider', ['slider' => $cmsPage->slider])
@stop

@section('introduction')
    @foreach ($cmsPage->getMedia('images') as $image)
        {!! Html::image($image->getUrl()) !!}
    @endforeach
    <section class="container introduction">
      <h1>{!! $cmsPage->title !!}</h1>
      <h2>{!! $cmsPage->subtitle !!}</h2>
      {!! $cmsPage->content !!}

      <a href="{!! url('/shop') !!}" title="Shop all products" class="button">Shop all</a>
    </section>
@stop

@section('content')
    @if($taxonomies->where('slug', 'earring-types')->first())
    <section class="container taxonomy">
        <header class="row">
            <h2>Shop By Earring Type</h2>
        </header>
        <ul class="row">
        @foreach($taxonomies->where('slug', 'earring-types')->first()->taxons as $taxon)
            <li class="taxon">
                <a href="{!! url('shop/categories/' . $taxon->slug) !!}" title="{!! $taxon->name !!}">
                <div>
                    <h4>{!! $taxon->name !!}</h4>
                </div>
                </a>
            </li>
        @endforeach
        </ul>
    </section>
    @endif
    @if($taxonomies->where('slug', 'collections')->first())
    <section class="container taxonomy">
        <header class="row">
            <h2>Shop By Collection</h2>
        </header>
        <ul class="row">
        @foreach($taxonomies->where('slug', 'collections')->first()->taxons as $taxon)
            @include('products.partials._taxon', ['taxon' => $taxon])
        @endforeach
        </ul>
    </section>
    @endif
  @foreach($cmsPage->nodes as $node)
      <section id="{!! $node->slug !!}" class="content-block cols-{!! $node->columns !!}">
          @foreach($node->nodeContents as $content)
              <div id="{!! $content->contentable->slug !!}" class="{!! $content->type !!}">
                  @include('pages.partials._'.$content->type, [$content->type => $content->contentable])
              </foreach>
          @endforeach
      </section>
  @endforeach
@stop

@section('inline-scripts')
@stop
