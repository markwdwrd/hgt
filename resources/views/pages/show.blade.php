@extends('layouts.master')

@section('page-title'){!! $cmsPage->title !!}@stop
@section('meta-title'){!! $cmsPage->meta_title !!}@stop
@section('meta-keywords'){!! $cmsPage->meta_keywords !!}@stop
@section('meta-description'){!! $cmsPage->meta_description !!}@stop
@section('page-id'){!! $cmsPage->slug !!}@stop
@section('main-class'){!! 'std-page' !!}@stop

@section('slider')
    @include('pages.partials._slider', ['slider' => $cmsPage->slider])
@stop

@section('introduction')
    <section class="container introduction">
        <div class="row">
            @if($cmsPage->hasMedia('images'))
                <div class="images">
                    @foreach ($cmsPage->getMedia('images') as $image)
                        {!! Html::image($image->getUrl()) !!}
                    @endforeach
                </div>
            @endif
            <div class="content">
                <h1>{!! $cmsPage->title !!}</h1>
                <h2>{!! $cmsPage->subtitle !!}</h2>
                {!! $cmsPage->content !!}
            </div>
        </div>
    </section>
@stop

@section('content')
    @foreach($cmsPage->nodes as $node)
        <section id="{!! $node->slug !!}" class="content-block cols-{!! $node->columns !!}">
            @foreach($node->nodeContents as $content)
                <div id="{!! $content->contentable->slug !!}" class="{!! $content->type !!}">
                    @include('pages.partials._'.$content->partial, [$content->type => $content->contentable])
                </div>
            @endforeach
        </section>
    @endforeach
@stop

@section('inline-scripts')
@stop
