@extends('layouts.master')

@section('page-title'){!! $cmsPage->title !!}@stop
@section('meta-title'){!! $cmsPage->meta_title !!}@stop
@section('meta-keywords'){!! $cmsPage->meta_keywords !!}@stop
@section('meta-description'){!! $cmsPage->meta_description !!}@stop
@section('page-id'){!! $cmsPage->slug !!}@stop
@section('main-class'){!! 'std-page' !!}@stop

@section('slider')
    @include('pages.partials._slider', ['slider' => $cmsPage->slider])
@stop

@section('introduction')
    <section class="container introduction">
        <div class="row">
            @if($cmsPage->hasMedia('images'))
                <div class="image">
                    @foreach ($cmsPage->getMedia('images') as $image)
                        {!! Html::image($image->getUrl()) !!}
                    @endforeach
                </div>
            @endif
            <div class="content">
                <h1>{!! $cmsPage->title !!}</h1>
                <h2>{!! $cmsPage->subtitle !!}</h2>
                {!! $cmsPage->content !!}

                {!! Form::open(['route' => 'enquiry', 'id' => 'contact-form', 'method' => 'POST']) !!}
                    {!! Form::text('name', null, ['id' => 'name', 'required', 'placeholder' => 'Name']) !!}
                    <span class="error">{!! $errors->first('name') !!}</span>

                    {!! Form::text('email', null, ['id' => 'email', 'required', 'placeholder' => 'Email']) !!}
                    <span class="error">{!! $errors->first('email') !!}</span>

                    {!! Form::text('phone', null, ['id' => 'phone', 'placeholder' => 'Phone']) !!}

                    {!! Form::textarea('message', null, ['id' => 'message', 'required', 'placeholder' => 'Message', 'rows' => '8']) !!}
                    <span class="error">{!! $errors->first('message') !!}</span>

                    <div class="g-recaptcha" data-sitekey="{!! env('INVISIBLE_RECAPTCHA_SITEKEY') !!}"></div>
                    <span class="error">{{ $errors->first('g-recaptcha-response') }}</span>

                    {!! Form::button('Submit', ['type' => 'submit', 'class' => 'button']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </section>
@stop

@section('content')
    @foreach($cmsPage->nodes as $node)
        <section id="{!! $node->slug !!}" class="content-block cols-{!! $node->columns !!}">
            @foreach($node->nodeContents as $content)
                <div id="{!! $content->contentable->slug !!}" class="{!! $content->type !!}">
                    @include('pages.partials._'.$content->type, [$content->type => $content->contentable])
                </foreach>
            @endforeach
        </section>
    @endforeach
@stop

@section('inline-scripts')
    <script type="text/javascript">
        $('#contact-form').validate({
            errorElement: 'span',
            rules: {
                email: {
                    email: true
                },
                name: {
                    required: true
                },
                message: {
                    required: true
                }
            }
        });
    </script>
@stop
