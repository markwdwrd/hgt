<div class="home">
    <div class="tilegroup">
        @if (count($tile_group->tile_1) > 0)
        <div class="tilegroup__slider">
            @foreach($tile_group->tile_1 as $tile_1)
                <div class="tilegroup__tile" style="background-image: url('{{ $tile_1->getFirstMediaUrl("image") }}')">
                    <h3>{{$tile_1->heading}}</h3>
                    @if ($tile_1->button_link)
                    <a href="{{$tile_1->button_link}}">
                        <button class="button" style="background-color: {{$tile_1->button_colour}}">{{$tile_1->button_text}}</button>
                    </a>
                    @endif
                </div>
            @endforeach
        </div>
        @else
            <div class="tilegroup__tile tilegroup__four" style="background-image: url('{{ $tile_1->getFirstMediaUrl("image") }}')">
                <h3>{{$tile_1->heading}}</h3>
                @if ($tile_1->button_link)
                <a href="{{$tile_1->button_link}}">
                    <button class="button" style="background-color: {{$tile_1->button_colour}}">{{$tile_1->button_text}}</button>
                </a>
                @endif
            </div>
        @endif


        @foreach($tile_group->tile_2 as $tile_2)
            <div class="tilegroup__tile tilegroup__two" style="background-image: url('{{ $tile_2->getFirstMediaUrl("image") }}')">
                <h3>{{$tile_2->heading}}</h3>
                <a href="{{$tile_2->button_link}}">
                    <button class="button full-button" style="background-color: {{$tile_2->button_colour}}">{{$tile_2->button_text}}</button>
                </a>
            </div>
        @endforeach

        @foreach($tile_group->tile_3 as $tile_3)
            <div class="tilegroup__tile tilegroup__one tilegroup__one-one" style="background-image: url('{{ $tile_3->getFirstMediaUrl("image") }}')">
                <h3>{{$tile_3->heading}}</h3>

                <a href="{{$tile_3->button_link}}">
                    <button class="button full-button" style="background-color: {{$tile_3->button_colour}}">{{$tile_3->button_text}}</button>
                </a>
            </div>
        @endforeach

        @foreach($tile_group->tile_4 as $tile_4)
            <div class="tilegroup__tile tilegroup__one tilegroup__one-two" style="background-image: url('{{ $tile_4->getFirstMediaUrl("image") }}')">
                <h3>{{$tile_4->heading}}</h3>

                <a href="{{$tile_4->button_link}}">
                    <button class="button full-button" style="background-color: {{$tile_4->button_colour}}">{{$tile_4->button_text}}</button>
                </a>
            </div>
        @endforeach
    </div>
</div>
