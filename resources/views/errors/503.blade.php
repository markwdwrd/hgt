<!DOCTYPE html>
<html>
    <head>
        <title>Coming Soon | {{ config('app.name') }}</title>
        <meta charset="utf8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
        <link rel="stylesheet" href="https://use.typekit.net/wdm5ogf.css">
        <style>
            html {
                position: relative;
                display: block;
                width: 100%;
                height: 100%;
            }
            body {
                text-align: center;
                background: rgb(255, 255, 250);
                color: rgb(20, 20, 19);
                font: 13px/1.4 futura-pt, sans-serif;
                letter-spacing: 1px;
                position: relative;
                display: block;
                width: 100%;
                height: 100%;
                margin: 0;
            }
            .content {
                overflow: auto;
                margin: 0 auto;
                position: absolute;
                top: 50%; left: 50%;
                transform: translate(-50%,-50%);
                width: 100%;
            }
            .logo {
                background-image: url('/img/hgt-logo.svg');
                background-size: contain;
                background-position: center center;
                background-repeat: no-repeat;
                text-indent: -999em;
                display: inline-block;
                width: 300px;
                height: 300px;
            }
            .text {
                padding: 0 1em;
                text-transform: uppercase;
            }
            h1 {
                display: block;
                text-indent: -999em;
    			height: 80px;
    			background-size: 60% auto;
                margin: 0 auto;
            }
            h2 {
                font: bold 18px/1.4 Arial, Helvetica, sans-serif;
                margin: 2em 0;
                text-transform: none;
            }
            h2 span {
                color: #fc4328;
            }
            h3 {
                font: bold 14px/1.4 Arial, Helvetica, sans-serif;
                margin: 0;
                text-transform: none;
                font-weight: 400;
            }
            img {
                height: 200px;
                width: 209.5px;
            }
        </style>
    </head>

    <body>
        <div class="content">
            <a href="#" class="logo" title="Hello. Good Thanks!">Hello. Good Thanks!</a>
            <div class="text">
                <p>We'll be back shortly!</p>
            </div>
        </div>
    </body>
</html>