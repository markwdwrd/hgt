<div class="order-coupon">
  {!! Form::open(['route' => 'checkout.coupon', 'method' => 'POST']) !!}
      {!! Form::label('coupon', 'Add a coupon') !!}
      <fieldset class="inline">
        {!! Form::text('coupon', null) !!}
        {!! Form::button('Apply', ['class' => 'button', 'type' => 'submit']) !!}
      </fieldset>
    {!! Form::close() !!}
</div>