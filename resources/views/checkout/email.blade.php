@extends('layouts.master')

@section('page-title'){!! 'Checkout' !!}@stop
@section('page-id'){!! 'checkout' !!}@stop
@section('main-class'){!! 'checkout email' !!}@stop

@section('content')
<div class="container">
    <div class="row">
        {!! Form::open(['url' => 'login', 'method' => 'POST']) !!}
            <header class="row">
                <h2>Login to account</h2>
            </header>
            @csrf
            <fieldset>
                {!! Form::label('email', 'Email') !!}
                {!! Form::text('email', null, ['id' => 'email', 'placeholder' => 'Email']) !!}
                <span class="error">{!! $errors->first('email') !!}</span>
            </fieldset>
            <fieldset>
                {!! Form::label('password', 'Password') !!}
                {!! Form::password('password', ['id' => 'password', 'class' => 'field-input', 'placeholder' => 'Password']) !!}
                <span class="error">{!! $errors->first('password') !!}</span>
            </fieldset>
            <label for="remember">
                <input type="checkbox" name="remember" id="remember" {!! old('remember') ? 'checked' : '' !!}>
                Remember Me
            </label>

            <fieldset>
                {!! Form::button('Login to Account', ['type' => 'submit']) !!}
                <p><a href={!! route('password.request') !!} title="Reset your password">Forgot your password?</a></p>
            </fieldset>
        {!! Form::close() !!}


        {!! Form::open(['route' => 'checkout.email', 'method' => 'POST']) !!}
            <header class="row">
                <h2>Guest Checkout</h2>
            </header>
            <fieldset>
                {!! Form::label('email', 'Email Address') !!}
                {!! Form::text('email', $order->email, ['placeholder' => 'Email']) !!}
                <span class="{!! $errors->has('email') ? 'error' : '' !!}">{!! $errors->first('email') !!}</span>
            </fieldset>
            <fieldset>
                {!! Form::button('Checkout as Guest', ['type' => 'submit']) !!}
                <p>Don't have an account? <a href="{!! url('register') !!}">Sign up for one here</a>.</p>
            </fieldset>
        {!! Form::close() !!}
    </div>
</div>
@stop


@section('inline-scripts')
@stop
