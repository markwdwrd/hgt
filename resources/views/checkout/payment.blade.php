@extends('layouts.master')

@section('main-class'){!! 'checkout payment' !!}@stop

@section('introduction')
    <section>
    </section>
@stop

@section('content')
<div class="container">
    <div class="row">
        {!! Form::open(['route' => 'checkout.payment', 'method' => 'POST', 'id' => 'checkout-payment']) !!}
            <header class="row">
                <h2>Select your payment method</h2>
            </header>
            <label>{!! Form::radio('payment_method', 'credit_card', ['checked']) !!} Credit Card</label>
            <label>{!! Form::radio('payment_method', 'paypal') !!} PayPal</label>
            @if($errors->has('payment_method'))
                <span class='error'>{!! $errors->first('payment_method') !!}</span>
            @endif
            <div class="payment-method" id="credit_card">
                <div id="error_display"></div>
                {!! Form::hidden('card_token', null, ['id' => 'card_token']) !!}
                <fieldset>
                    {!! Form::label('card_name', 'Name on Card') !!}
                    {!! Form::text('card_name', null, ['id' => 'card_name']) !!}
                </fieldset>

                <fieldset>
                    {!! Form::label('card_number', 'Credit Card Number') !!}
                    <div id="card_number" class="stripe-field"></div>
                </fieldset>
                <div class="row">
                <fieldset>
                    {!! Form::label('card_expiry', 'Expiry') !!}
                    <div id="card_expiry"  class="stripe-field"></div>
                </fieldset>
                <fieldset>
                    {!! Form::label('card_cvc', 'CVC') !!}
                    <div id="card_cvc"  class="stripe-field"></div>
                </fieldset>
                </div>
            </div>

            <div class="payment-method" id="paypal" hidden>
                <div class="message">
                    <p>You will be redirected to PayPal to complete your purchase.</p>
                    <p>Do not close your browser until after you have been redirected back to this site.</p>
                </div>
            </div>
            {!! Form::button('Process Payment', ['class' => 'button full-button', 'type' => 'submit']) !!}
        {!! Form::close() !!}

        @include('checkout.partials._totals')
    </div>
</div>
@stop


@section("inline-scripts")
    <script src="https://js.stripe.com/v3/"></script>
    <script type="text/javascript">
        $(document).on('change', 'input[name=payment_method]', function (e) {
            $('.payment-method').hide();
            $('.payment-method#' + e.target.value).show();
        });

        const params = {
            style: {
                base: {
                    backgroundColor: '#ffffff',
                    color: '#141413',
                    fontWeight: '500',
                    fontFamily: 'futura-pt, sans-serif',
                    fontSize: '14px',
                }
            }
        };

        const stripe = Stripe('{!! env('STRIPE_KEY') !!}');
        const elements = stripe.elements();
        const cardNumber = elements.create('cardNumber', params);
        const cardExpiry = elements.create('cardExpiry', params);
        const cardCvc = elements.create('cardCvc', params);;

        cardNumber.mount('#card_number');
        cardExpiry.mount('#card_expiry');
        cardCvc.mount('#card_cvc');

        const form = document.getElementById('checkout-payment');

        let submitting = false;

        form.addEventListener('submit', function(e) {

            e.preventDefault();

            if (submitting) return false;
            submitting = true;
            $(form).addClass('submitting');

            $('#error_display').html('');
            let payment_method = $('input[name=payment_method]:checked').val();

            if (payment_method == 'credit_card') {
                stripe.createToken(cardNumber).then(e => {
                    if(e.token !== undefined) {
                        submit(e.token.id);
                    }
                    else {
                        $('#error_display').html(e.error.message);
                        submitting = false;
                        $(form).removeClass('submitting');
                    }
                });
            } else {
                form.submit();
            }
        });

        function submit(token) {
            $('#card_token').val(token);
            form.submit();
        }
    </script>
@stop
