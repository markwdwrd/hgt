@extends('layouts.master')

@section('page-title'){!! $category->name !!}@stop
@section('meta-title'){!! $cmsPage->meta_title !!}@stop
@section('meta-keywords'){!! $cmsPage->meta_keywords !!}@stop
@section('meta-description'){!! $cmsPage->meta_description !!}@stop
@section('page-id'){!! $category->slug !!}@stop
@section('main-class'){!! 'products category' !!}@stop

@section('slider')
    @include('pages.partials._slider', ['slider' => $cmsPage->slider])
@stop

@section('introduction')
    @foreach ($cmsPage->getMedia('images') as $image)
        {!! Html::image($image->getUrl()) !!}
    @endforeach

    <section class="container">
        <h1>{!! $category->name !!}</h1>
        {!! $cmsPage->content !!}
        @include('categories.partials._breadcrumbs')
    </section>

@stop

@section('content')
    @if(count($category->children))
        <nav class="container">
            <div class="row">
                <div class="categories">
                @foreach($category->children as $taxonomy)
                    @if (count($taxonomy->taxons))
                        <fieldset>
                        <label for="{!! $taxonomy->slug !!}">Shop by {!! $taxonomy->name !!}</label>
                        {!! Form::select($taxonomy->slug, ['' => 'All'] + $taxonomy->taxons->pluck('name', 'slug')->toArray(), null, []) !!}
                        </fieldset>
                    @endif
                @endforeach
                </div>
                {!! $products->render('vendor.pagination.default') !!}
            </div>
        </nav>
    @endif

    <section class="container">
        <ul class="row">
        @foreach($products as $product)
            @include('products.partials._product', ['product' => $product])
        @endforeach
        </ul>
        {!! $products->render('vendor.pagination.default') !!}
    </section>
@stop

@section('inline-scripts')
<script type="text/javascript">
  $(document).on('change', '.categories select', function (e) {
    const url = "{!! url('shop/categories/') !!}";
    window.location = url + '/' + e.target.value;
  });
</script>
@stop