<footer>
    <div class="container">
        <div class="row">
            <a href="{!! url('/') !!}" class="logo" title="{!! env("APP_NAME") !!}">{!! env("APP_NAME") !!}</a>
            <p class="copyright">&copy; Copyright {!! Carbon\Carbon::now()->format('Y') !!} Hello Good Thanks AU.</p>
        </div>
    </div>
</footer>