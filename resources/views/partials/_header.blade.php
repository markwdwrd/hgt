<header>
    <div class="container">
        <div class="row">
            <a href="{!! url('/') !!}" class="logo" title="{!! env("APP_NAME") !!}">{!! env("APP_NAME") !!}</a>
            @include('partials._nav')
            @include('partials._user')
        </div>
    </div>
    @include('partials._messages')
</header>
