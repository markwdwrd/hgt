
<nav class="site">
    @if(count($menus) > 0)
        <ul>
            @foreach($menus as $menu)
                @if($menu->show_in_nav)
                    <li><a href="{{ url($menu->slug) }}">{{ $menu->label }}</a>
                    {!! View::make('partials.menus._menu')->with(['children' => $menu->children, 'currentMenu' => $menu]) !!}
                @endif
            @endforeach
        </ul>
    @endif
</nav>