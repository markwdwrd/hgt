<!DOCTYPE html>
<html lang="{!! app()->getLocale() !!}" @yield('item-scope')>
	<head>
		<title>@if (View::hasSection('page-title'))@yield('page-title') | @endif{!! config('app.name') !!}</title>
		<meta charset="utf8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
		<meta name="title" content="@yield('meta-title')">
		<meta name="keywords" content="@yield('meta-keywords')">
		<meta name="description" content="@yield('meta-description')">
		<meta name="csrf-token" content="{!! csrf_token() !!}">
		<script src='https://www.google.com/recaptcha/api.js'></script>
		@yield('meta-other')

		<link rel="stylesheet" href="{!! mix('/css/vendor.css') !!}">
		<link rel="stylesheet" href="{!! mix('/css/app.css') !!}">
	</head>

	<body id="@yield('page-id')">
		@yield('slider')

		@include('partials._header')

		<main class="@yield('main-class')">
			@yield('slider')
			@yield('introduction')
			@yield('content')
		</main>

		@include('partials._footer')

		<div class="overlay"></div>

		<script src="{!! mix('/js/manifest.js') !!}"></script>
		<script src="{!! mix('/js/vendor.js') !!}"></script>
		<script src="{!! mix('/js/app.js') !!}"></script>

		{!! HTML::script('/js/jqvalidate-additional-methods.js') !!}

		<!-- Polyfill for ES6 Promises for IE11, UC Browser and Android browser support -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
			@yield('inline-scripts')
	</body>
</html>
