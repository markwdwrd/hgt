<?php

namespace App\Http\Controllers;

use App\Models\CmsPage;
use App\Models\Taxonomy;

class HomeController extends Controller
{
    /**
     * Show home page.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $cmsPage = CmsPage::where('slug', 'home')->firstOrFail();

        $taxonomies = Taxonomy::isType('product')->get();

        return view('pages.home', compact('cmsPage', 'taxonomies'));
    }
}
